//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab #9
// Date: 11/05/2015
//
// Purpose: To create program that randomly plays a selected piece of music based on the user's input.

package lab9;
import org.jfugue.player.Player;
import org.jfugue.pattern.Pattern;
import java.util.*;
import java.io.*;

public class MusicProgram 
{
		public static void main (String args []) throws IOException{
		// create a list to store elements from the file
        String musicList [] = new String[5];
        
        // create a file
        File file = new File("music");
        Scanner scan = new Scanner (file);

        // iterate through the file and save the elements into list
        int i=0;
        while(i<musicList.length)
        {
            musicList[i]= scan.nextLine();
            i++;
        }
		
        // random number
        Random random = new Random();
        int index = random.nextInt(musicList.length);  
        System.out.println(index);
       
        // variable dictionary 
        String instrumentString, tempoString; 
       
        // choose instrument 
        Scanner scanner = new Scanner(System.in); // needed for user input 
        System.out.println("Daniel Gray\nLab 9\n" + new Date() + "\n");
        System.out.println("Please type your choice of an instrument.");
        System.out.println("Please use underscores to designate selections if instrument name is more than one word.");
        System.out.println("Your choices are piano, guitar, saxophone, organ, or trumpet.");
        instrumentString = scanner.next();
        System.out.println("Please enter your preferred tempo.");
        tempoString = scanner.next();
        
     
        Player player = new Player();
        String playString = "I["+instrumentString+"] T["+tempoString+"] "+musicList[index];
        System.out.println(playString);
        Pattern pattern = new Pattern(playString);
        player.play(pattern);   
        
        System.out.println("Would you like to repeat the music selection?");
        String repeat;
        Scanner keyboard = new Scanner(System.in);  
        System.out.println("Please enter yes or no");
            repeat = keyboard.nextLine();
        if(repeat.equals("yes"))
        {
             Pattern pattern1 = new Pattern(playString);
             player.play(pattern1); 
       	   
        }
        else if(repeat.equals("no"))
        {
        	 System.out.println("Thanks for playing!");  
        	 System.exit(0);
        }
         
           
	}
}
