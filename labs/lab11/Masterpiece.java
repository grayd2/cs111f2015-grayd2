//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.
//
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11/18/2015
// Purpose: Practice writing Java classes and methods, and using loops.
//=================================================
import java.awt.*;
import javax.swing.*;

public class Masterpiece extends JApplet
{
    // instance variables
    private int width, height, startAngle, arcAngle, origin_x, origin_y;
    private Graphics page;

    // constructor
    public Masterpiece(Graphics p, int x, int y, int x1, int y1, int s, int a)
    {
        width = x;
        height = y;
        startAngle = s;
        arcAngle = a;
        origin_x = x1;
        origin_y = y1;
        page = p;

    }

    // methods
    public void drawPacman() // draws Pacman
    {
        int x1 = 100;
        int y1 = 100;
        int x = 150;
        int y = 150;
        int s = 30;
        int a = 300;
        page.setColor(Color.yellow);
        page.fillArc(x1,y1,x,y,s,a);
    }
    public void drawEyes() // draws Pacman's eyes
    {
        int x1 = 175;
        int y1 = 125;
        int x = 10;
        int y = 10;
        int s = 5;
        int a = -360;
        page.setColor(Color.black);
        page.fillArc(x1,y1,x,y,s,a);
    }
    public void drawDots() // draws Pac-dots in a straight line using a loop
    {
        for (int i = 0; i < 10; i++)
        {
            int x1 = 250;
            int y1 = 160;
            int x = 25;
            int y = 25;
            page.setColor(Color.yellow);
            page.fillOval(x1*i, y1, x, y);
        }

    }
    public void drawMazeTop() // draws a rectangular border in a straight line on the top of drawing
    {
        int x1 = 0;
        int y1 = 10;
        int x = 2500;
        int y = 35;
        page.setColor(Color.blue);
        page.fillRect(x1,y1,x,y);
    }
    public void drawMazeBottom() // draws a rectangular border in a straight line on the bottom of the drawing
    {
        int x1 = 0;
        int y1 = 260;
        int x = 2500;
        int y = 35;
        page.setColor(Color.blue);
        page.fillRect(x1,y1,x,y);

    }
}
