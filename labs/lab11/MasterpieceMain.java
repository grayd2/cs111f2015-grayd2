//=================================================
// Honor Code: The work I am submitting is
// a result of my own thinking and efforts.
//
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 11
// Date: 11/18/2015
//
// Purpose: Practice writing Java classes and methods, and using loops.
//=================================================
import java.awt.*;
import javax.swing.*;

public class MasterpieceMain extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page)
    {
        Masterpiece master = new Masterpiece(page, 300, 300, 300, 300, 300, 300);
        master.drawPacman();
        master.drawEyes();
        master.drawDots();
        master.drawMazeTop();
        master.drawMazeBottom();
    }
    // main method that runs the program
    public static void main(String[] args)
    {
            JFrame window = new JFrame(" My Drawing "); // adds drawing canvas
      		window.getContentPane().add(new MasterpieceMain());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            window.setVisible(true); // makes window appear on the screen
		    window.setSize(600, 400); // sets size of window
    }
}
