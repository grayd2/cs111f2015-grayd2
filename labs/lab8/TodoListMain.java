import java.io.IOException; // needed to print out error messages
import java.util.Scanner;
import java.util.Iterator;
import java.util.Date; // needed for printing today's date

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Daniel Gray\nLab 8\n" + new Date() + "\n");
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, completed, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        while(scanner.hasNext()) { // creates loop which searches for user input and returns requested information
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            else if(command.equals("completed")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsCompleted(chosenId);
            }
            else if(command.equals("priority-search")){
                 System.out.println("What priority would you like to search?");
                 String p = scanner.next();
                 Iterator it = todoList.findTasksOfPriority(p);
                 while(it.hasNext()) {
                     System.out.println(it.next());
                 }
            }
            else if(command.equals("category-search")){
                System.out.println("What category would you like to search?");
                String c = scanner.next();
                Iterator it = todoList.findTasksOfCategory(c);
                while(it.hasNext()){
                    System.out.println(it.next());
                }
            }
            else if(command.equals("quit")) {
                break; // ends loop and exits program
            }
        }

    }

}
