import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException; // needed to print out error messages

public class TodoList {

    // INSTANCE VARIABLES:
    private ArrayList<TodoItem> todoItems;
    private static final String TODOFILE = "todo.txt";

    // CONSTRUCTOR:
    //------------------------------------
    // Constructs a TodoList and assigns todoItems to a new arraylist
    //------------------------------------
    public TodoList() {
        todoItems = new ArrayList<TodoItem>();
    }

    // METHODS:

    //------------------------------------
    // Creates addTodoItem
    //------------------------------------
    public void addTodoItem(TodoItem todoItem) {
        todoItems.add(todoItem);
    }
    //------------------------------------
    // Creates an iterator which returns todoItems
    //------------------------------------
    public Iterator getTodoItems() {
        return todoItems.iterator();
    }
    //------------------------------------
    // Creates method which reads through the TodoItem file
    //------------------------------------
    public void readTodoItemsFromFile() throws IOException {
        Scanner fileScanner = new Scanner(new File(TODOFILE));
        while(fileScanner.hasNext()) { // creates loop which scans through the file and sets strings
            String todoItemLine = fileScanner.nextLine();
            Scanner todoScanner = new Scanner(todoItemLine);
            todoScanner.useDelimiter(",");
            String priority, category, task;
            priority = todoScanner.next();
            category = todoScanner.next();
            task = todoScanner.next();
            TodoItem todoItem = new TodoItem(priority, category, task);
            todoItems.add(todoItem);
        }
    }
    //----------------------------------
    // Creates a method which will tell if a task is completed and sets return type as int
    //----------------------------------
    public void markTaskAsCompleted(int toMarkId) {
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) { // creates a loop which gets the id of the task and will marks as complete
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getId() == toMarkId) {
                todoItem.markCompleted();
            }
        }
    }
    //---------------------------------
    // Creates an iterator called findTasksOfPriority and returns type string
    //---------------------------------
    public Iterator findTasksOfPriority(String requestedPriority) {
        ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>(); // creates a new array list of TodoItem
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) { // creates a loop which returns the requested priority and adds it to string
                TodoItem todoItem = (TodoItem)iterator.next();
                if(todoItem.getPriority().equals(requestedPriority))
                    priorityList.add(todoItem);

        }
        return priorityList.iterator();

    }
    //---------------------------------
    // Creates an iterator called findTasksOfCategory and returns type string
    //---------------------------------
    public Iterator findTasksOfCategory(String requestedCategory) {
        ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) { // creates a loop which returns the requested category and adds it to string
            TodoItem todoItem = (TodoItem)iterator.next();
            if(todoItem.getCategory().equals(requestedCategory))
                categoryList.add(todoItem);


        }
        return categoryList.iterator();
    }
    //-------------------------------
    // Creates a method of type String
    //-------------------------------
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        Iterator iterator = todoItems.iterator();
        while(iterator.hasNext()) {
            buffer.append(iterator.next().toString());
            if(iterator.hasNext()) {
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }

}
