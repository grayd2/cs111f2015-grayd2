public class TodoItem {

    // INSTANCE VARIABLES:
    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    // CONSTRUCTOR:
    // --------------------------------
    // Sets the initial value for variables
    // --------------------------------
    public TodoItem(String p, String c, String t) {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    // METHODS:

    //--------------------------------
    // Returns the id of variable type int
    //--------------------------------
    public int getId() {
        return id;
    }
    //--------------------------------
    // Returns the priority of priority String
    // -------------------------------
    public String getPriority() {
        return priority;
    }
    //--------------------------------
    // Returns the category of category String
    //--------------------------------
    public String getCategory() {
        return category;
    }
    //--------------------------------
    // Returns the task of task String
    // -------------------------------
    public String getTask() {
        return task;
    }
    //--------------------------------
    // Marks if the user inputed task is completed
    //--------------------------------
    public void markCompleted() {
        done = true;
    }
    //--------------------------------
    // Returns that the user inputed task is completed
    //--------------------------------
    public boolean isCompleted() {
        return done;
    }
    //---------------------------------
    // Returns all strings and if inputed task was completed
    //---------------------------------
    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", completed? " + done);
    }

}
