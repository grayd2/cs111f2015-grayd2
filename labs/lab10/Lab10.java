
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 10
// Date: 11/10/2015
//
// Purpose: To develop a program that prints a symmetrical carpet design using for loops, and/or do while loops,
// nested loops and to continue to develop problem solving skills.
//**************
import java.util.Date; // needed for printing today's date

public class Lab10
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String[] args)
	{
        System.out.println("Daniel Gray\nLab 10\n" + new Date() + "\n");

        // creates checkerboard pattern on top
        int num = 1;
        int amount = 1;
        String a = args[0];

        while ( num <= 4 )
        {
            while ( amount <= 15 )
            {
                System.out.print( a+" " );
                amount += 1;
            }
            amount = 1;
            System.out.println("\n");
            num += 1;
        }

        // creates diamond
        for (int i = 1; i <= 5; i++) {

            for (int s = 15; s > i; s--) {
                System.out.print(" ");
            }
            for (int j = 1; j < i; j++) {
                System.out.print(a);
            }
            for (int j = 1; j < i; j++) {
                System.out.print(a);
            }
                System.out.println("");
        }
        for (int i = 1; i <= 5; i++) {

            for (int s = -9; s < i; s++) {
                System.out.print(" ");
            }
            for (int j = 5; j > i; j--) {
                System.out.print(a);
            }
            for (int j = 5; j > i; j--) {
                System.out.print(a);
            }
            System.out.println("");

        }

        // creates stars
        int row = 10, col = 15;
        for(int x=0; x<row-1; x++)
     {
        if ( x % 2 == 0){

        for(int y=0; y<col; y++)

            System.out.print( a+" ");
        }
        else{

            System.out.print(" ");

        for(int y=0; y<col-1; y++){

            System.out.print( a+" ");
            }
        }

            System.out.println();

        }

        // creates pyramid
        int d = 15;
        int s = d, m;
        for (int i = 5; i <= d; i++){
            m = s;
			while (s > 1) {
				System.out.print(" ");
                s--;
			}
			for (int j = 5; j <= i; j++) {
				System.out.print( a+" ");
			}
			System.out.print("\n");
			s = m - 1;
        }

        // creates rectangle
       for(int i = 0;i <= 10; i++){
            for(int j = 0; j <= 28;j++){
            if(i==0 || i==28)
                System.out.print(a);
            else{
          if(j==0 || j==28)
          System.out.print(a);
          else
          System.out.print(" ");
            }
        }
    System.out.print("\n");
    }

        // creates checkerboard pattern on bottom
        while ( num <= 8 )
        {
            while ( amount <= 15 )
            {
                System.out.print( a+" " );
                amount += 1;
            }
            amount = 1;
            System.out.println("\n");
            num += 1;
        }

    }
}

