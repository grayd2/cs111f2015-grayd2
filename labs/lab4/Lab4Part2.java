
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 4
// Date: 09/24/2015
//
// Purpose: To calculate the square root through guessing
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed for user input

public class Lab4Part2
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String[] args)
	{
        System.out.println("Daniel Gray\nLab 4\n" + new Date() + "\n");

        //Variable dictionary
        double x; //value to find square root of
        double guess; //estimated value of square root
        Scanner scan = new Scanner(System.in); // used for input

        System.out.println("Find the square root of what value?");
        x = scan.nextDouble();

        System.out.println("Enter initial guess for square root");
        guess = scan.nextDouble();

        guess = (guess + (x / guess)) * .5;

        guess = (guess + (x / guess)) * .5;

        System.out.println("The original value was " + x);
        System.out.println("The final value of the guess is " + guess);
        System.out.println("The value of the guesses " + (guess*guess));
     }

}
