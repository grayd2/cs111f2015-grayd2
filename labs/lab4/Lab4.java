//=================================================
// Honor Code: This work is mine unless otherwise cited.

// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 4
// Date: 09/18/2015
//
// Purpose: To write a program that prints out the correct number
// of ten dollar bills, five dollar bills, one dollar bills,
// quarters, dimes, nickels, and pennies; given a positive integer.
//=================================================
import java.util.Date;
import java.util.Scanner;

    public class Lab4{

    public static void main(String[] args)
    {
        System.out.println("Daniel Gray\nLab 4\n" + new Date() + "\n");

        Scanner sc = new Scanner(System.in);

        double amount;
        int ten;
        int five;
        int one;
        int quarter;
        int dime;
        int nickel;
        int totalCents;

            System.out.println("Please enter the amount which you would like to be changed.");
            amount = sc.nextDouble();

            totalCents=(int)amount;
            totalCents=(int)(100*amount);

            ten=(totalCents/1000);
            five=(int) ((amount-(ten*10))/5);
            one=(int) ((amount-(ten*10)-(five*5))/1);
            quarter=(int) ((amount-(ten*10)-(five*5)-(one*1))/.25);
            dime=(int) ((amount-(ten*10)-(five*5)-(one*1)-(quarter*.25))/.10);
            nickel=(int) ((amount-(ten*10)-(five*5)-(one*1)-(quarter*.25)-(dime*.10))/.05);




            System.out.println("Ten dollar bills: " + ten);
            System.out.println("Five dollar bills: " + five);
            System.out.println("One dollar bills: " + one);
            System.out.println("Quarters: " + quarter);
            System.out.println("Dimes: " + dime);
            System.out.println("Nickels: " + nickel);



    }
}












