//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab #7
// Date: 10/21/2015
//
// Purpose: To create a Sudoku puzzle validator for 4x4 grids made up of 2x2 regions
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // needed for user input

public class SudokuMain
{
    public static void main (String args[])
    {

         //Label output with name and date:
        System.out.println("Daniel Gray\nLab 7\n" + new Date() + "\n");

        SudokuChecker foo = new SudokuChecker();
        foo.getGrid();
        foo.checkGrid();
    }
}
