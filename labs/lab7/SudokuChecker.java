import java.util.Scanner;

public class SudokuChecker
{

    Scanner scan = new Scanner (System.in);
        private int w1;
        private int w2;
        private int w3;
        private int w4;
        private int x1;
        private int x2;
        private int x3;
        private int x4;
        private int y1;
        private int y2;
        private int y3;
        private int y4;
        private int z1;
        private int z2;
        private int z3;
        private int z4;

        public SudokuChecker()
        {
            System.out.println("Welcome to the Sudoku Checker!");
            System.out.println("");
            System.out.println("This program checks simple, small, 4x4 Sudoku grids for correctness.");
            System.out.println("");
            System.out.println("Each column, row and 2x2 region contains the numbers 1 through 4 only once.");
            System.out.println("");
            System.out.println("To check your Sudoku, enter your board one row at a time, with each digit separated by a space. Hit ENTER at the end of a row.");
        }

        public void getGrid ()
        {
            System.out.println();
            System.out.print ("Enter Row 1: ");
            w1 = scan.nextInt();
            w2 = scan.nextInt();
            w3 = scan.nextInt();
            w4 = scan.nextInt();

            System.out.print ("Enter Row 2: ");
            x1 = scan.nextInt();
            x2 = scan.nextInt();
            x3 = scan.nextInt();
            x4 = scan.nextInt();

            System.out.print ("Enter Row 3: ");
            y1 = scan.nextInt();
            y2 = scan.nextInt();
            y3 = scan.nextInt();
            y4 = scan.nextInt();

            System.out.print ("Enter Row 4: ");
            z1 = scan.nextInt();
            z2 = scan.nextInt();
            z3 = scan.nextInt();
            z4 = scan.nextInt();
        }
        public void checkGrid ()
        {
            int var = 0;
            System.out.println(); // Region

            if ( w1 + w2 + x1 + x2 == 10)
            {
               System.out.println ("REG-1:GOOD");
            }
            else
            {
                System.out.println ("REG-1:BAD");
                var = var + 1;
            }

            if ( y1 + y2 + z1 + z2 == 10)
            {
               System.out.println ("REG-2:GOOD");
            }
            else
            {
               System.out.println ("REG-2:BAD");
               var = var + 1;
              }

            if ( w3 + w4 + x3 + x4 == 10)
            {
               System.out.println ("REG-3:GOOD");
            }
            else
            {
               System.out.println ("REG-3:BAD");
               var = var + 1;
            }

            if ( y3 + y4 + z3 + z4 == 10)
            {
               System.out.println ("REG-4:GOOD");
            }
            else
            {
               System.out.println ("REG-4:BAD");
               var = var + 1;
            }

           System.out.println(); // Row

           if ( w1 + w2 + w3 + w4 == 10)
           {
               System.out.println ("ROW-1:GOOD");
           }
           else
           {
               System.out.println ("ROW-1:BAD");
               var = var + 1;
           }

           if ( x1 + x2 + x3 + x4 == 10)
           {
               System.out.println ("ROW-2:GOOD");
           }
           else
           {
               System.out.println ("ROW-2:BAD");
               var = var + 1;
           }

           if ( y1 + y2 + y3 + y4 == 10)
           {
               System.out.println ("ROW-3:GOOD");
           }
           else
           {
               System.out.println ("ROW-3:BAD");
               var = var + 1;
           }

           if ( z1 + z2 + z3 + z4 == 10)
           {
               System.out.println ("ROW-4:GOOD");
           }
           else
           {
               System.out.println ("ROW-4:BAD");
               var = var + 1;
           }

           System.out.println(); // Column

           if ( w1 + x1 + y1 + z1 == 10)
           {
               System.out.println ("COL-1:GOOD");
           }
           else
           {
               System.out.println ("COL-1:BAD");
               var = var + 1;
           }

           if ( w2 + x2 + y2 + z2 == 10)
           {
               System.out.println ("COL-2:GOOD");
           }
           else
           {
               System.out.println ("COL-2:BAD");
               var = var + 1;
           }

           if ( w3 + x3 + y3 + z3 == 10)
           {
               System.out.println ("COL-3:GOOD");
           }
           else
           {
               System.out.println ("COL-3:BAD");
               var = var + 1;
           }

           if ( w4 + x4 + y4 + z4 == 10)
           {
               System.out.println ("COL-4:GOOD");
           }
           else
           {
               System.out.println ("COL-4:BAD");
               var = var + 1;
           }

           System.out.println(); // Flag

           if ( var == 0)
           {
               System.out.println ("SUDOKU:VALID");
           }
           else
           {
             System.out.println ("SUDOKU:INVALID");
           }
           System.out.println();
        }

}






