//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab 2
// Date: 09 04 2015
//
// Purpose: To convey two words of wisdom. 
//**************
import java.util.Date; // needed for printing today's date

public class WordsofWisdoms2
{
	//-------------------
	//main method: program execution begins here 
	//-------------------
	public static void main(String[] args)
	{
	  //Label output with name and date:
	  System.out.print("Daniel Gray\n Lab 2\n" + new Date() + "\n");
	  System.out.print("I'll be back");
	  System.out.print("You shall not pass");
	}
}
