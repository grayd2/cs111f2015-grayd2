
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// Dustin Cuscino
// CMPSC 111 Fall 2015
// Lab 5
// Date: 09/25/2015
//
// Purpose: Create a Java program that manipulates a DNA string by finding its
// complement and performing different types of mutation.
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; //needed for user input
import java.util.Random;

public class DnaManipulation
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String[] args)
    {

       // Variable dictionary:
         Scanner scan = new Scanner(System.in); // used for input
         Random rand = new Random(); // random number generator
         String dnaString, complementString, mutationString, deleteString, changeString; //dnaString = input by user
         int length; // length of dnaString
         int location; // one of positions in dnaString
         char c; // a letter chosen randomly from a string

        System.out.println("Daniel Gray and Dustin Cuscino\nLab 5\n" + new Date() + "\n");


        System.out.println("Please enter a DNA string using only A, C, G, and T");
        dnaString = scan.next();

        complementString = dnaString.replace("A", "t") // gives the complement for the dnaString
                   .replace("T", "a")
                   .replace("C", "g")
                   .replace("G", "c")
                   .toUpperCase();
       System.out.println("Complement is " + complementString);


        int randomNumber = rand.nextInt(4);
        location = rand.nextInt(dnaString.length());
        mutationString = dnaString.substring(0,location) + "ACGT".charAt(randomNumber) + dnaString.substring(location); // inserts random from list of accepted letter in random position
            System.out.println("Inserting " + dnaString.charAt(randomNumber) + " at position " + location + " gives "
                    + mutationString);

        location = rand.nextInt(dnaString.length());
        deleteString = dnaString.substring(0,location) + dnaString.substring(location + 1); // removes random letter from random position
        System.out.println("Deleting from position " + location + " gives "
                    + deleteString);

         location = rand.nextInt(dnaString.length());
         changeString = dnaString.substring(0,location) + "ACGT".charAt(randomNumber) + dnaString.substring(location + 1); // combines the two codes above and replaces letter in string to a randomly chosen letter from the list of accepted letters
         System.out.println("Changing position " + location + " with " + "ACGT".charAt(randomNumber) + " gives " +
                    changeString);


    }


}







