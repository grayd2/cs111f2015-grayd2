//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Lab #3 
// Date: 09/11/2015
//
// Purpose to calculate the tip and the total bill for the user and each person's share of the restaurant bill
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class GratuityCalculator{

     public static void main(String[] args){


	Scanner sc = new Scanner(System.in); 

	System.out.println("Daniel Gray, CMPSC 111, Lab #3\n" + new Date() + "\n");

	System.out.println("Welcome to the gratuity calculator!\nPlease enter your name"); 

	String name = sc.nextLine(); 

	System.out.println("Hi "+name+"\nEnter in your bill total");

	double bill = sc.nextDouble(); 

	System.out.println("Enter in the percentage you would like to tip (0-1)");

	double tip = sc.nextDouble(); 

	double tipAmount = bill*tip;

	double total = bill*(1+tip); 

	System.out.println("Your bill is: $"+bill+"\nYour gratuity is: $"+tipAmount+"\nYour total bill is: $"+total);

	System.out.println("How many people are helping to pay?"); 

	int people = sc.nextInt();

	double portion = total/people; 

	System.out.println("Each person's portion is: $"+portion); 

	System.out.println("Thank you for using our calculator");

   }


}
