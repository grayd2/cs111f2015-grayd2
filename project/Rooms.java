import java.util.ArrayList;

class Rooms {

    public static void build(Room[][] room, final int WIDTH, final int HEIGHT) {

    	// Initialize rooms
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                room[i][j] = new Room(i, "", "", null);
            }
        }

        room[0][0].setNumber(1);
        room[0][0].setName("Forest");
        room[0][0].setDescription("You are enveloped on all sides by the forest, the huge branches of the trees. Old beyond guessing, they seem. Great trailing beards of lichen hung from them, blowing and swaying in the breeze.");

        room[0][1].setNumber(2);
        room[0][1].setName("Forest");
        room[0][1].setDescription("You discover a skeleton hidden underneath the foliage, with a skeleton key. How fortunate!");
        room[0][1].setItems("Skeleton Key");

        room[1][0].setNumber(3);
        room[1][0].setName("Forest");
        room[1][0].setDescription("The trees grumble and creak, almost if they were talking to each other.");

        room[2][0].setNumber(4);
        room[2][0].setName("Forest");
        room[2][0].setDescription("You start thinking to yourself that you haven't seen a single animal since you woke up.");

        room[3][0].setNumber(5);
        room[3][0].setName("Forest");
        room[3][0].setDescription("The forest appears to be endless.");

        room[3][1].setNumber(6);
        room[3][1].setName("Forest");
        room[3][1].setDescription("You are awed by the beauty of the ancient woodland and rest awhile on the mossy ground. You come across some mushrooms. ");
        room[3][1].setItems("Mushrooms");

        room[2][1].setNumber(7);
        room[2][1].setName("Forest");
        room[2][1].setDescription("You discover a large stone altar on top of a grassy knoll, perhaps used by Druids some forgotten time ago, while searching you find a small sacrificial dagger ");

        room[1][1].setNumber(8);
        room[1][1].setName("Forest");
        room[1][1].setDescription("A beam of sunlight brushes against your cheek. ");

        room[0][2].setNumber(9);
        room[0][2].setName("Forest");
        room[0][2].setDescription("You come across a fairy ring, you find it oddly mesmerizing. The story goes that it's a portal to another realm. ");

        room[1][2].setNumber(10);
        room[1][2].setName("Forest");
        room[1][2].setDescription("There is a little stream running through here, you take a sip of the icy water.");

        room[2][2].setNumber(11);
        room[2][2].setName("Forest");
        room[2][2].setDescription("It looks like there is a some sort of trapdoor here.");

        room[3][2].setNumber(12);
        room[3][2].setName("Forest");
        room[3][2].setDescription("You come across an ancient garden, the dilapidated stone pillars and fountains have been overrun by ivy and lichen. ");



    }
    public static void print(Room[][] room, int x, int y) {

    	System.out.println();
        System.out.println(room[x][y].getDescription());
        System.out.println("You see: " + room[x][y].getItems());
    }

    // Remove item from room when added to inventory
    public static void removeItem(Room[][] room, int x, int y, String item) {

    	room[x][y].deleteItem(item);
    }
}

class Room {

    private int number;
    private String name;
    private String description;
    public ArrayList<String> items = new ArrayList<>();

    public Room(int number, String name, String description,
            ArrayList<String> items) {
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return this.number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setItems(String item) {
        this.items.add(item);
    }

    public void deleteItem(String item) {
        this.items.remove(item);
    }

    public ArrayList<String> getItems() {
        return this.items;
    }

}
