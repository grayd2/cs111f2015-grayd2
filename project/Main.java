import java.util.ArrayList;

public class Main {

    public static void main(String args[]) {

        // Build rooms
        final int WIDTH = 4;
        final int HEIGHT = 3;
        Room[][] room = new Room[WIDTH][HEIGHT];
        Rooms.build(room, WIDTH, HEIGHT);
        int x = 0;
        int y = 0;
        int score = 0;

        // Load inventory
        ArrayList<String> inventory = new ArrayList<>();

        // Title Screen
        System.out.println("");
        System.out.println("████████╗██╗  ██╗███████╗     ██████╗ ██████╗ ███████╗ █████╗ ████████╗   ");
        System.out.println("╚══██╔══╝██║  ██║██╔════╝    ██╔════╝ ██╔══██╗██╔════╝██╔══██╗╚══██╔══╝   ");
        System.out.println("   ██║   ███████║█████╗      ██║  ███╗██████╔╝█████╗  ███████║   ██║      ");
        System.out.println("   ██║   ██╔══██║██╔══╝      ██║   ██║██╔══██╗██╔══╝  ██╔══██║   ██║      ");
        System.out.println("   ██║   ██║  ██║███████╗    ╚██████╔╝██║  ██║███████╗██║  ██║   ██║      ");
        System.out.println("   ╚═╝   ╚═╝  ╚═╝╚══════╝     ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝      ");
        System.out.println("██╗   ██╗███╗   ██╗██████╗ ███████╗██████╗  ██████╗ ██████╗  ██████╗ ██╗   ██╗███╗   ██╗██████╗  ");
        System.out.println("██║   ██║████╗  ██║██╔══██╗██╔════╝██╔══██╗██╔════╝ ██╔══██╗██╔═══██╗██║   ██║████╗  ██║██╔══██╗ ");
        System.out.println("██║   ██║██╔██╗ ██║██║  ██║█████╗  ██████╔╝██║  ███╗██████╔╝██║   ██║██║   ██║██╔██╗ ██║██║  ██║ ");
        System.out.println("██║   ██║██║╚██╗██║██║  ██║██╔══╝  ██╔══██╗██║   ██║██╔══██╗██║   ██║██║   ██║██║╚██╗██║██║  ██║ ");
        System.out.println("╚██████╔╝██║ ╚████║██████╔╝███████╗██║  ██║╚██████╔╝██║  ██║╚██████╔╝╚██████╔╝██║ ╚████║██████╔╝ ");
        System.out.println(" ╚═════╝ ╚═╝  ╚═══╝╚═════╝ ╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚═════╝  ");
        System.out.println("███████╗███╗   ███╗██████╗ ██╗██████╗ ███████╗ ");
        System.out.println("██╔════╝████╗ ████║██╔══██╗██║██╔══██╗██╔════╝ ");
        System.out.println("█████╗  ██╔████╔██║██████╔╝██║██████╔╝█████╗   ");
        System.out.println("██╔══╝  ██║╚██╔╝██║██╔═══╝ ██║██╔══██╗██╔══╝   ");
        System.out.println("███████╗██║ ╚═╝ ██║██║     ██║██║  ██║███████╗ ");
        System.out.println("╚══════╝╚═╝     ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝ ");
        System.out.println("");
        System.out.println("You wake up in a forest, faraway from home, unsure of where you are and exactly how you got here. ");
        System.out.println("The light grows broader as you move on, and soon you see a rock-wall before you: the side of a hill ... the sun is falling full on its stony face.");
        System.out.println("The twigs of the trees at its foot are stretched out stiff and still, as if reaching out to the warmth.");
        System.out.println("Where all had looked so shabby and grey before, the wood now gleamed with rich browns, and with the smooth black-greys of bark like polished leather.");
        System.out.println("The boles of the trees glowed with a soft green like young grass: early spring or a fleeting vision of it was about them.");
        System.out.println("");
        System.out.println("It will soon be getting late.");
        System.out.println("");

        System.out.println("What will you do?");

    	// Print starting room description
    	Rooms.print(room, x, y);

        // Start game loop
        boolean playing = true;
        while (playing) {

        	// Get user input
            String input = Input.getInput();

            // Movement commands

            if (input.equals("n")) {
                if (y > 0) {
                    y--;
                    Rooms.print(room, x, y);
                } else {
                    System.out.println("The path is blocked.");
                }
            } else if (input.equals("s")) {
                if (y < HEIGHT - 1) {
                    y++;
                    Rooms.print(room, x, y);
                } else {
                    System.out.println("The path is blocked.");
                }
            } else if (input.equals("e")) {
                if (x > 0) {
                    x--;
                    Rooms.print(room, x, y);

                } else {
                    System.out.println("The path is blocked.");
                }
            } else if (input.equals("w")) {
                if (x < WIDTH - 1) {
                    x++;
                    Rooms.print(room, x, y);
                } else {
                    System.out.println("The path is blocked.");
                }
            }

            // Look commands
            else if (input.equals("look")) {
                Rooms.print(room, x, y);
            }

            // Get commands
            else if (input.length() > 4  && input.substring(0, 4).equals("get ")) {
            	if (input.substring(0, input.indexOf(' ')).equals("get")) {
            		if (input.substring(input.indexOf(' ')).length() > 1) {
            			String item = input.substring(input.indexOf(' ') + 1);
                    	score = Inventory.checkItem(x, y, item, inventory, room, score);
            		}
            	}
            }

            //Open commands
            else if (input.equals("open trapdoor")) {
                if(inventory.contains("Skeleton Key")){
                    System.out.println("The door reluctantly opens to reveal a rickety staircase descending into darkness.\nYou slowly go down but suddenly the trap door crashes shut, and you hear someone barring it.");
                    score += 200;
                    System.out.println("The entrance was like a sort of arch leading into a gloomy tunnel made by two great trees that leant together,\ntoo old and strangled with ivy and hung with lichen to bear more than a few blackened leaves.");
                    System.out.println("The path itself was narrow and wound in and out among the trunks.");
                    System.out.println("You bravely go onwards, deeper into the void.");
            	    System.out.println("Score: " + score + "/500");
                    System.exit(0);
                }
                else {
                    System.out.println("You don't have the key!");
                }
           }

            // Inventory commands
            else if (input.equals("i") || input.equals("inv")
                    || input.equals("inventory")) {
                Inventory.print(inventory);
            }

            else if (input.equals("score")) {
            	System.out.println("Score: " + score + "/500");
            }

            else if (input.equals("restart")) {
            	System.out.println();
            	Main.main(args);
            }

            else if (input.equals("help")) {
            	System.out.println("Type 'n'/'e'/'s'/'w' to move around");
            	System.out.println("Type 'look' for a description of the room you're in");
            	System.out.println("Type 'get' + the item to get something");
            	System.out.println("Type 'i' to view your inventory");
                System.out.println("Type 'open [object]' to explore new places!");
            	System.out.println("Type 'score' to view your score");
            	System.out.println("Type 'restart' to restart the game");
            	System.out.println("Type 'quit' to quit the game");
            }

            // Quit commands
            else if (input.equals("quit")) {
                System.out.println("Goodbye!");
                playing = false;

            // Catch-all for invalid input
            }else {
                System.out.println("You can't do that.");
            }
        }
        System.exit(0);
    }
}
