
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Class Exercise  
// Date: 09/16/2015
//
// Purpose: To create a program that converts Fahrenheit to Celsius 
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; 

public class TempConversion
{
	//-------------------
	//main method: program execution begins here 
	//-------------------
	public static void main(String[] args)
	{
		Scanner userInput = new Scanner(System.in);
		float tempFahrenheit; 
		float tempCelsius; 
		System.out.println("Please enter temperature in Fahrenheit"); 
		tempFahrenheit = userInput.nextFloat(); 
		tempCelsius = (tempFahrenheit-32)*(float)5/9; 
		System.out.println("The temperature in Celsius is : " + tempCelsius);
		


	}
}
