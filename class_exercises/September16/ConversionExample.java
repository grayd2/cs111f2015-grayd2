
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray 
// CMPSC 111 Fall 2015
// Class Exercise
// Date: 09/16/2015
//
// Purpose: Get the inputs from the user, manipulates it and output the result.
//**************
import java.util.Scanner; 
public class ConversionExample
{
	//-------------------
	//main method: program execution begins here 
	//-------------------
	public static void main(String[] args)
	{
		int num1, num2; 
		float num3; 

		Scanner input = new Scanner(System.in);

		// get the input from the user 
		System.out.println("Enter an integer"); 
		num1 = input.nextInt(); 
		System.out.println("Enter another integer"); 
		num2 = input.nextInt(); 
		System.out.println("Enter any number"); 
		num3 = input.nextFloat(); 


		System.out.println("Arithmetic result 1: "+(num1*num2+num3)); 
		System.out.println("Arithmetic result 2: "+(num1*num2+(int)num3)); 
		System.out.println("Arithmetic result 3: "+(int)(num1*num2+num3)); 


		System.out.println("Last digit  "+(num1%10));
		System.out.println("Last digit float: "+(int)(num3%10));


		
	}
}
