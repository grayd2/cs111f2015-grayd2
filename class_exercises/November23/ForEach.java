public class ForEach
{
    public static void main(String args[])
    {
        String[] cities = {"Tokyo", "Hong Kong", "Seoul"};

        for(String str : cities)
            {
                System.out.println("I would like to visit " + str);
            }

     }
}
