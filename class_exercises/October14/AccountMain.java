public class AccountMain
{
    public static void main (String [] args)
    {
        // create an instance of Account
        Account JoeSmith = new Account(500);
        Account JJ = new Account(200);
        Account BradSmith = new Account(100);

        // call methods
        System.out.println("Balance: "+JoeSmith.getBalance());
        System.out.println("Balance of JJ "+JJ.getBalance());
        System.out.println("Balance of BradSmith "+BradSmith.getBalance());

        JoeSmith.setBalance(1000);
        System.out.println("Balance: "+JoeSmith.getBalance());

        JoeSmith.setBalance(-10);
        System.out.println("Balance: "+JoeSmith.getBalance());

        JJ.setBalance(50);
        System.out.println("Balance of JJ: "+JJ.getBalance());

    }
}
