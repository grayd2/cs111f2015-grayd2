
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Class Exercise
// Date: 09/09/2015
//
// Purpose: To use variables in expressions, use Scanner 
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; 
public class AgeStats
{
	//-------------------
	//main method: program execution begins here 
	//-------------------
	public static void main(String[] args)
	{
	  //Label output with name and date:
	  System.out.println("Daniel Gray \n" + new Date() + "\n");

	  double ageInYears; // declaration 
	  double result; 
	  String name;  
	
	  // declare and create an instance of a class 
	  Scanner userInput = new Scanner (System.in);

	  System.out.println("Please enter your name");
	  name = userInput.next();

	  System.out.println("You entered name");

	  System.out.println("Please enter your age");

	  ageInYears = userInput.nextDouble();

	  System.out.println("You entered "+ageInYears);

	  // convert age in years to minutes 
	  result = ageInYears*365*24*60; 

	  System.out.println("The age in minutes is "+result);

	  // convert age in years to centuries 
	  result = ageInYears/100;

	  System.out.println("The age in centuries is "+result);
	
	}
}
