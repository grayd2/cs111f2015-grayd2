import javax.swing.*;
import java.awt.*;

public class ActionTester
{
    public static void main (String[] args)
    {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton button = new JButton("Button");
        JTextField text = new JTextField("Initial text");
        JLabel label = new JLabel("Initial label");

        button.addActionListener(new SetTextListener(text,label));

        frame.add(button);
        frame.add(text);
        frame.add(label);

        frame.pack();
        frame.setVisible(true);
    }
}
