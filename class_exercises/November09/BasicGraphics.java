import javax.swing.*;
import java.awt.*;

public class BasicGraphics extends JApplet // inherit
{
    public static void main(String [] args)
    {
        JFrame window = new JFrame("Dan's drawing");
        window.getContentPane().add(new BasicGraphics()); // main method and drawing method are in the same class
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // allow user to close window and stop the program
        window.setVisible(true); // displays pop-up window to user
        window.setSize(600,400); // width and length in pixels

    }

    public void paint (Graphics g) // method which gets called when you link graphics
    {
        g.setColor(Color.red); // sets color as red
        g.fillRect(10,20,40,40); // fillRect (x,y,x2,y2), x2 is the width, and y2 is the height
        // the coordinate system is your computer screen
        g.setColor(Color.yellow);
        g.fillOval(100,100,100,100);
    }
}
