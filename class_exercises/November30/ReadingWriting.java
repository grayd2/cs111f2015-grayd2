import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadingWriting
{
    public static void main (String args []) throws IOException
    {
        ArrayList<String> list = new ArrayList<String>();
        String searchWord;
        int count;

        // create a file
        File file = new File("text.txt");
        Scanner scan = new Scanner(file);

        while(scan.hasNext())
        {
            String word = scan.next();
            list.add(word);
        }
        System.out.println("List size : "+list.size());

        searchWord = "Beowulf";
        count = 0;

        for(String myWord : list)
        {
            if(myWord.equals(searchWord))
            {
                count++;
            }
        }

        System.out.println("Count: "+count);

        try{
            PrintWriter printer = new PrintWriter("output.txt");
            printer.println(searchWord+": "+count);
            printer.close();
        }
        catch(Exception e)
        {
             System.out.println("File not found");
        }
    }
}
