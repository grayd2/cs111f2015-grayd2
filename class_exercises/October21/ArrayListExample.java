import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class ArrayListExample
{
    public static void main (String args []) throws IOException
    {
        // create a list to store elements from the file
        ArrayList<String> twitterList = new ArrayList<String>();

        // create a file
        File file = new File("words.txt");
        Scanner scan = new Scanner (file);

        // iterate through the file and save the elements into list
        while(scan.hasNext())
        {
            twitterList.add(scan.next());
        }
        System.out.println(twitterList);

        // remove all words ending in 'e'
        int count = 0;

        while(count < twitterList.size())
        {
            String word = twitterList.get(count);
            if(word.endsWith("e"))
            {
                twitterList.remove(count);
            }
            count++;
        }
        System.out.println(twitterList);
    }
}
