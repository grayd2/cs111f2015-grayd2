public class Calculator
{
    private double result;

    public double calculate(double x, char op, double y)
    {
        switch(op)
        {
            case '+':
                result = x+y;
                break;
            case '-':
                result = x-y;
                break;
            case '*':
                result = x*y;
                break;
            case '/':
                result = x/y;
                break;
            case '%':
                result = x%y;
                break;
            default:
                System.out.println("Invalid operator");
        }
        return result;
    }
}
