//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Date: 11/02/2015
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // needed for user input


public class CalculatorMain
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String args[])
	{
        System.out.println("Daniel Gray\nClass Exercise\n" + new Date() + "\n");
        double x = Double.parseDouble(args[0]);
        char operator = args[1].charAt(0);
        double y = Double.parseDouble(args[2]);

        Calculator calc = new Calculator();
        System.out.println("Result: "+calc.calculate(x,operator,y));




        double left, right;
        char op; // operator
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a simple expression: "); // spaces before and after
        left = input.nextDouble();
        op = input.next().charAt(0); // take character from position 0
        right = input.nextDouble();

        System.out.println("You entered: "+left+op+right);

        System.out.println("Your result is: "+calc.calculate(left,op,right));


	}
}
