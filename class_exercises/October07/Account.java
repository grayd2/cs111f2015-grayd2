//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Date: 10/07/2015
//**************
import java.util.Date; // needed for printing today's date

public class Account
{

    //instance variable:
    private double balance;
    //method that changes the balance
    //set method
    public void setBalance(double amount)
    {
        balance += amount;
    }

    //method return the balance
    //get method
    public double getBalance()
    {
        return balance;
    }


}
