public class AccountMain
{
    public static void main(String [] args)
    {
        //create an instance of Account
        Account a1 = new Account();
        //call methods
        System.out.println("Balance:" +a1.getBalance());
        a1.setBalance(1000);
        System.out.println("Balance:" +a1.getBalance());

        a1.setBalance(-10);
        System.out.println("Balance:" +a1.getBalance());

    }
}
