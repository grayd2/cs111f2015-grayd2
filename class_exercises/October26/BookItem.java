public class BookItem
{
    private String title;
    private String author;
    private float price;

    public BookItem(String t, String a, float p)
    {
        title = t;
        author = a;
        price = p;
    }

    public float getPrice()
    {
        return price;
    }

    public String makeString()
    {
        return new String(title+", "+author+", "+price);
    }
}
