import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class BookListMain
{
    public static void main(String [] args) throws IOException
    {
        System.out.println("Welcome! You can read, search, exit");

        Scanner scan = new Scanner(System.in);
        BookList booklist = new BookList();

        while(scan.hasNext())
        {
            String input = scan.next();
            if(input.equals("read"))
            {
                booklist.readFromFile();
            }
            else if(input.equals("search"))
            {
                Iterator it = booklist.search();
                while(it.hasNext())
                {
			BookItem item = (BookItem)it.next();                    
			System.out.println(item.makeString());
                }
            }
            else if(input.equals("exit"))
            {
                break;
            }
        }
    }
}
