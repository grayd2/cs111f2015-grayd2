import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class BookList
{
    private ArrayList<BookItem> bookItems;
    private static final String BOOKFILE = "amazonList.txt";

    public BookList()
    {
        bookItems = new ArrayList<BookItem>();
    }

    public void readFromFile() throws IOException
    {
        File file = new File(BOOKFILE);
        Scanner fileScan = new Scanner(file);
        while(fileScan.hasNext())
        {
            String bookLine = fileScan.nextLine();
            Scanner scan = new Scanner (bookLine);
            scan.useDelimiter(",");
            String title = scan.next();
            String author = scan.next();
            String price = scan.next();
            BookItem bookitem = new BookItem(title, author, Float.parseFloat(price));
            bookItems.add(bookitem);
        }
    }

    public Iterator search()
    {
        ArrayList<BookItem> myList = new ArrayList<BookItem>();
        Iterator it = bookItems.iterator();
        while(it.hasNext())
        {
            BookItem b = (BookItem)it.next();
            if(b.getPrice()>2 && b.getPrice()<10)
            {
                myList.add(b);
            }
        }
        return myList.iterator();
    }
}
