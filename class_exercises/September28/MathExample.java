import java.util.Scanner;

public class MathExample
{
    public static void main(String [] args)
    {

        double value;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number:");
        value = input.nextDouble();

        System.out.println("Absolute value: "+Math.abs(value));
        System.out.println("Floor: "+Math.floor(value));
        System.out.println("Square root: "+Math.sqrt(value));
        System.out.println("Pi: "+Math.PI);


    }





}


