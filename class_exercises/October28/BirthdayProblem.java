import java.io.*;
import java.util.Scanner;

public class BirthdayProblem
{
    public static void main (String args []) throws IOException
    {
        String bd [] = new String [47];
        String seen[] = new String[47];

        File file = new File("birthdays.txt");
        Scanner input = new Scanner(file);

        int count = 0;
        while(input.hasNext())
        {
            String birthday = input.next();
            bd[count] = birthday;
            count++;
        }

        int i=0, j=0;

        while(i<bd.length)
        {
            while(j<seen.length)
            {
                if(bd[i].equals(seen[j]))
                {
                    System.out.println("Duplicate birthday "+bd[i]);
                    break;
                }
                j++;
            }
            seen[i] = bd[i];
            System.out.println("birthday "+seen[i]);
            i++;
            j = 0;
        }
    }
}
