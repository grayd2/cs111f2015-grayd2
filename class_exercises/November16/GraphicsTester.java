/**
 * A simple class to experiment with Swing graphics
 */

import javax.swing.*;
import java.awt.*;

public class GraphicsTester {
	public static void main(String [] args)
	{
		JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // kills the program

		frame.setLayout(new FlowLayout());
		frame.add(new JButton("I'm a JButton"));
		frame.add(new JTextField("I'm a JTextField"));
		frame.add(new JLabel("I'm a JLabel"));
		frame.add(new JSlider());
		frame.add(new JProgressBar());


        JPanel panel = new JPanel(); // creates instance of JPanel
        panel.setLayout(new GridLayout(2,2)); // parameters: rows, columns
        panel.add(new JButton("JButton panel"));
        panel.add(new JLabel("Panel components"));
        panel.add(new JTextField("textfield"));
        panel.add(new JButton("another button"));
        frame.add(panel);


        frame.pack(); // fits everything in pop-out window
		frame.setVisible(true);
	}
}
