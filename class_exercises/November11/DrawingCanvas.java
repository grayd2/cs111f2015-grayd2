import javax.swing.*;
import java.awt.*;

public class DrawingCanvas
{
    private int width, height;
    private Graphics page;

    public DrawingCanvas(Graphics p, int x, int y)
    {
        width = x;
        height = y;
        page = p;
    }

    public void drawSnow()
    {
        page.setColor(Color.white);
        page.fillRect(0, 350, 600, 50);
    }

    public void drawSun()
    {
         page.setColor(Color.yellow);
         page.fillOval(0,0,50,50);

         page.drawLine(70,30,100,50);
         page.drawLine(30,70,50,100);
         page.drawLine(50,50,100,100);
         page.drawLine(70,10,150,15);
         page.drawLine(10,70,10,130);
    }

    public void drawTree(int x, int y)
    {
         // draw trunk
        page.setColor(Color.gray);
        page.fillRect(x,y,30,150);

        // leaves
        page.setColor(Color.green);
        page.fillOval(x-20,y-100,125,125); // right
        page.fillOval(x-50,y-70,115,115); // left
        page.fillOval(x-50,y-100,125,125); // left
        page.fillOval(x-20,y-70, 115,115);//right
        page.fillOval(x-40,y-100,115,115);
    }
}
