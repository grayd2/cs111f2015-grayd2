import java.util.Random;

public class RandomExample{
    public static void main(String[] args)
    {
        int randomInt;
        float randomFloat;

        Random rand = new Random();

        randomInt = rand.nextInt();
        System.out.println("A random integer :"+randomInt);

        randomInt = rand.nextInt(100);
        System.out.println("A random integer 0-100: "+randomInt);

        randomFloat = rand.nextFloat();
        System.out.println("A random float: "+randomFloat);
    }

}
