//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray and Dustin Cuscino
// CMPSC 111 Fall 2015
// Class Example Program
// Date: September 14, 2015
//
// Purpose is to find the sum of two integers.
// The variables in the program are the integers entered.
// The types of variables are integers.
// The values stored in the variables in the beginning are 0 and at the end the values are the numbers we chose to input into the program. 
//*************************************

import java.util.Scanner;

public class Addition
{
	public static void main( String args[] )
	{
		
		int first = 2; 
		int second = 0; 
		double sum = 10;
		int count = 5;
		
		Scanner input = new Scanner( System.in );

		System.out.print( "Enter first integer: " );
		first = input.nextInt(); 

		System.out.print( "Enter second integer: " );
		second = input.nextInt();
		
		sum += (first % second);
		System.out.println( "Sum is " +sum );
	
		sum = ++count + ++count + +count++;
		System.out.println("New sum is "+sum);
		System.out.println("New count is "+count);

		sum = 0;
		count = 5; 

		sum = count++ + ++count + ++count;
		System.out.println("New sum is "+sum);
		System.out.println("New count is "+count);

	}
}
