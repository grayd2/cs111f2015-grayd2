
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray 
// CMPSC 111 Fall 2015
// Practical 3
// Date: 09/11/2015
//
// Purpose: To implement a "Mad Libs" Program
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class MadLib
{

	//----------------------------
	// main method: program execution begins here 
	//----------------------------
	private static Scanner in = new Scanner(System.in);

	public static void main(String[] args)
	{
	System.out.println("What's your name?"); 
	String name = in.nextLine();
	System.out.println("What's your favorite color?"); 
	String color = in.nextLine();
	System.out.println("Name an animal");
	String animal = in.nextLine();

	madlib(name,animal,color);
	}
	public static void madlib(String name, String animal, String color)
	{
	System.out.print("Hi, my name is " + name); 
	System.out.print(" Let me tell you a story, one day when I was walking down the street,");
	System.out.print("and I saw a very cute " + animal + " " + "on the sidewalk! It had the prettiest coat of " + color);
	System.out.print(" and I went over and said hello."); 
	}
}





