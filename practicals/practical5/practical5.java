
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Practical 5
// Date: 10/02/2015
//
// Purpose : To write a program that, given the user's input, determines if it is a leap year, emergence of Brood II, or predicted to be a peak year of sunspot activity
//**************
import java.util.Date; // needed for printing today's date
import java.util.Scanner;

public class practical5
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String[] args)
	{
    Scanner input = new Scanner (System.in);
    System.out.println ("Enter a year between 1000 and 3000: ");
      //year we want to check
        int year = input.nextInt();

      //if year is divisible by 4, it is a leap year

            if((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
                    System.out.println("Year " + year + " is a leap year");
            else
                    System.out.println("Year " + year + " is not a leap year");
    }
    {
        if(year == 17)
                System.out.println("")





    }
}
