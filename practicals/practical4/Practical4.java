/************************************
  Honor Code: This work is mine unless otherwise cited.
  Daniel Gray
  CMPSC 111
  25 September 2015
  Practical 4

  Basic Input with a dialog box
 ************************************/
import java.util.Random;
import javax.swing.JOptionPane;

public class Practical4
{
    public static void main ( String[] args )
    {

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "Let's get you a new identity!" );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog(" What is your first name?");

        //create a message with the modified first name
        String newName = "Your new first name is "+ name+"-chan";

        //prompt user to enter the last name
        String surname = JOptionPane.showInputDialog(" What is your last name?");

        //create a message with the modified last name
        String newSurname = " Your new last name is " + surname + "-san";


        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName + newSurname);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name

        // prompt user to enter the age
        int ans = Integer.parseInt(JOptionPane.showInputDialog("Enter your age"));
        Random ran = new Random();
        int num;
        num = ran.nextInt(20);


        // modify the age
        String newAge = "Your age is: "+(ans-num); // TO DO: change age using a random number

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        // prompts user to enter salary
        int salary = Integer.parseInt(JOptionPane.showInputDialog("Enter your salary"));
        Random r = new Random();
        int num2;
        num2 = r.nextInt();
        // modify salary
        String newSalary = "Your new salary is: "+(salary+num2);
        //display a message with new salary
        JOptionPane.showMessageDialog(null, newSalary);

        // prompts user for savings
        int savings = Integer.parseInt(JOptionPane.showInputDialog("Enter your savings"));
        Random rr = new Random();
        int num3;
        num3 = rr.nextInt(9999999);
        // modifies savings
        String newSavings = "Your new savings is: "+(savings+num3);
        //displays a message with new savings
        JOptionPane.showMessageDialog(null, newSavings);


        // TO DO: come up with your own (at least two) questions and answers

    } //end main
}  //end class Practical4
