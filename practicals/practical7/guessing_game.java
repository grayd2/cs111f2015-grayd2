
//***************
// Honor Code: This work is mine unless otherwise cited.
// Daniel Gray
// CMPSC 111 Fall 2015
// Date: 10/23/2015
//**************
import java.util.Date; // needed for printing today's date
import java.util.Random; // needed for random number to guess
import java.util.Scanner; // needed for user input

public class guessing_game
{
	//-------------------
	//main method: program execution begins here
	//-------------------
	public static void main(String[] args)
	{
	  //Label output with name and date:
        System.out.println("Daniel Gray\nPractical 7\n" + new Date() + "\n");

        Random rand = new Random();
        int numberToGuess = rand.nextInt(100);
        int numberOfTries = 0;
        Scanner input = new Scanner (System.in);
        int guess;
        boolean win = false;

        while (win == false)
        {
            System.out.println("Guess a number between 1 and 100");
            guess = input.nextInt();
            numberOfTries++;

            if (guess == numberToGuess)
            {
                win = true;
            }
            else if (guess < numberToGuess)
            {
                System.out.println("Your guess was too low");
            }
            else if (guess > numberToGuess)
            {
                System.out.println("Your guess was too high");
            }
        }

        System.out.println("Congratulations, you've bested me");
        System.out.println("The number was " + numberToGuess);
        System.out.println("It took you " + numberOfTries + " tries");


	}
}
