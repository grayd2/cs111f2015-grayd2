
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

class KmToMiles {
    public static void main(String[] args) {
        JFrame window = new KmToMilesGUI();
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
    }
}


class KmToMilesGUI extends JFrame {
    private JTextField m_milesTf      = new JTextField(10);
    private JTextField m_kilometersTf = new JTextField(10);
    private JButton    m_convertBtn   = new JButton("Convert");


    public KmToMilesGUI() {
        JPanel content = new JPanel();
        content.setLayout(new FlowLayout());
        content.add(new JLabel("Kilometers"));
        content.add(m_kilometersTf);
        content.add(m_convertBtn);
        content.add(new JLabel("Miles"));
        content.add(m_milesTf);
        this.setContentPane(content);
        this.pack();

        m_convertBtn.addActionListener(new ConvertBtnListener());

        this.setTitle("Kilometers to Miles");
    }

    class ConvertBtnListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String kmStr = m_kilometersTf.getText();
            double km = Double.parseDouble(kmStr);
            double mi = convertKmToMi(km);
            m_milesTf.setText("" + mi);                           }
    }


    public static double convertKmToMi(double kilometers) {
        double miles = kilometers * 0.621;
        return miles;
    }
}
