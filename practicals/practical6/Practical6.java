//====================================
// CMPSC 111
// Practical 6
// 15--16 October 2015
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical6
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Octopus odd;            // second octopus
        Octopus age;
        Utensil spat;           // a kitchen utensil
        Utensil fork;

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky");    // create and name the octopus
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.


        fork = new Utensil("fork");
        fork.setColor("red");
        fork.setCost(100);

        odd = new Octopus("Odd");
        odd.setWeight(100);
        odd.setUtensil(fork);


        // get methods for odd
        System.out.println("Testing 'get' methods:");
        System.out.println(odd.getName() + " weighs " +odd.getWeight()
                + " pounds\n" + "and is " + odd.getAge()
            + " years old. His favorite utensil is a "
            + odd.getUtensil());

        System.out.println(odd.getName() + "'s " + odd.getUtensil() + " costs $"
                + odd.getUtensil().getCost());
        System.out.println("Utensil's color: " + fork.getColor());

        // set methods for odd
        odd.setAge(40);
        odd.setWeight(135);
        fork.setCost(16.99);
        fork.setColor("green");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(odd.getName() + "'s new age: " + odd.getAge());
        System.out.println(odd.getName() + "'s new weight: " + odd.getWeight());
        System.out.println("Utensil's new cost: $" + fork.getCost());
        System.out.println("Utensil's new color: " + fork.getColor());

        // get methods for ocky
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // set methods for odd
        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());



        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

    }
}
