//****************
// Daniel Gray
// Pratical 2 "Something Interesting"
// 4 September 2015
// Prints something interesting
//****************
import java.util.Date;
public class Somethinginteresting
{
  public static void main(String[] args)
  {
    System.out.println("Daniel Gray, CMPSC 111\n" +new Date() + "\n");
    System.out.println(" ________");
    System.out.println("|       \\  ______");
    System.out.println("|         ||      |__    _");
    System.out.println("|         ||  _   |  |  | |");
    System.out.println("|         || |	| |   | | |");
    System.out.println("|    |)   || |__| |    |  |");
    System.out.println("|         ||  __  |  _	  |");
    System.out.println("|         || |	| | | |   |");
    System.out.println("|         || |	| | |  |  |");
    System.out.println("|_________/|_|  |_|_|   |_|");
  }
}         
