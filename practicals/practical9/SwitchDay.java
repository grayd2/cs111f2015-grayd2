//*********************************************************************************
// CMPSC 111 Fall 2015
// Practical 9
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        int day = s.nextInt();

switch (day)
  {
    case 1 :
    case 7 : System.out.print("This is a weekend day");
             break;
    case 2 :
    case 3 :
    case 4 :
    case 5 :
    case 6 : System.out.print("This is a weekday");
             break;
    default : System.out.print("Not a legal day");
              break;
  }


    // TO DO: Using a switch statement, given the day of the week, print whether it is a weekday or a weekend day
  // DONE
    }
}
